import java.util.Scanner;

public class Main{
    private static Scanner reader = new Scanner(System.in);
    public static void main(String[] args) {
        Robot robot = new Robot();
        int goX = reader.nextInt();
        int goY = reader.nextInt();
        robot.move(goX, goY);
    }

}
