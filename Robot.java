/**
 * Класс, соджержащий поля и методы для работы с роботом,
 * которому нужно попасть домой
 *
 * @author Комиссарова М.Е., 16IT18K
 */

class Robot {
    private int x;
    private int y;
    private Directions direction;

    /**
     * Конструктор для робота
     * @param x начальная координата х
     * @param y начальная координата у
     * @param direction начальное направление взгляда
     */
    private Robot(int x, int y, Directions direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    Robot(){
        this(0, 0, Directions.UP);
    }

    /**
     * Перемещает робота к заданной координате
     *
     * @param goX X координата, в которую должен прийти робот
     * @param goY Y координата, в которую должен прийти робот
     */
    void move(int goX, int goY) {
        if (goY > y) {
            while (direction != Directions.UP) {
                turnLeft();
            }
            do {
                stepForward();
            } while (goY != y);
        } else if (goY < y) {
            while (direction != Directions.DOWN) {
                turnRight();
            }
            do {
                stepForward();
            } while (goY != y);
        }
        if (goX > x) {
            while (direction != Directions.RIGHT) {
                turnRight();
            }
            while (goX != x) {
                stepForward();
            }
        } else if (goX < x) {
            while (direction != Directions.LEFT) {
                turnLeft();
            }
            do {
                stepForward();
            } while (goX != x);
        }

    }

    /**
     * Поворачивает робота налево на 90 градусов
     */
    private void turnLeft() {
        switch (direction) {
            case DOWN:
                System.out.println("поворот налево");
                direction = Directions.RIGHT;
                break;
            case LEFT:
                System.out.println("поворот налево");
                direction = Directions.DOWN;
                break;
            case RIGHT:
                System.out.println("поворот налево");
                direction = Directions.UP;
                break;
            case UP:
                System.out.println("поворот налево");
                direction = Directions.LEFT;
                break;
            default:
                break;
        }
    }

    /**
     * Поворачивает робота направо на 90 градусов
     */
    private void turnRight() {
        switch (direction) {
            case DOWN:
                System.out.println("поворот направо");
                direction = Directions.LEFT;
                break;
            case LEFT:
                System.out.println("поворот направо");
                direction = Directions.UP;
                break;
            case RIGHT:
                System.out.println("поворот направо");
                direction = Directions.DOWN;
                break;
            case UP:
                System.out.println("поворот направо");
                direction = Directions.RIGHT;
                break;
            default:
                break;
        }
    }

    /**
     * Делает один шаг в направлении взгляда робота
     */
    private void stepForward() {
        switch (direction) {
            case DOWN:
                --y;
                System.out.println("шаг вниз" + "(" + x + ";" + y + ")");
                break;
            case LEFT:
                --x;
                System.out.println("шаг влево" + "(" + x + ";" + y + ")");
                break;
            case RIGHT:
                ++x;
                System.out.println("шаг вправо" + "(" + x + ";" + y + ")");
                break;
            case UP:
                ++y;
                System.out.println("шаг вверх" + "(" + x + ";" + y + ")");
                break;
            default:
                break;
        }
    }
}

